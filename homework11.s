	.equ NULL, 0
	.global main
	
main:

@Display your first and last name

	LDR R0, =name
	BL printf

@Open file output.txt

	LDR R0, =path2
	LDR R1, =mode2
	BL fopen
	MOV R4, R0	@ Save the file pointer
	CMP R4, #NULL	@ Did we get an error?
	BNE _next

@ Display error message

	LDR R0, =error_on_open
	BL printf
	BAL _exit

_next:

@Open file input.txt

	LDR R0, =path1
	LDR R1, =mode1
	BL fopen
	MOV R5, R0	@ Save the file pointer
	CMP R5, #NULL	@ Did we get an error?
	BNE _read
	
@ Display error message

	LDR R0, =error_on_open
	BL printf
	BAL _exit

@Read file input.txt a line at a time

_read:

	LDR R0, =line
	LDR R2, =end_of_line
	SUB R1, R2, R0	@ Calculate the size of the buffer line
	MOV R2, R5
	BL fgets
	CMP R0, #NULL
	BEQ _close


@For each line read from input.txt
@-decode the line

	MOv R9, #0
	
_loop:
	
@do

	LDRB R8, [R0, R9]
	
@if (R8 = 10)
	
	CMP R8, #10
	BEQ _display
	
@if (R8 = 65)

	CMP R8, #65
	BEQ _add
	
@if (R8 < 65)

	BLT _store
	
@if (R8 < 78)

	CMP R8, #78
	BLT _add
	
@if (R8 < 91)

	CMP R8, #91
	BLT _subtract
	
@if (R8 < 110)

	CMP R8, #110
	BLT _add

@else

	BAL _subtract
	
	
_add:
	
	ADD R8, #13
	BAL _store
	
_subtract:

	SUB R8, #13
	
_store:

	STRB R8, [R0, R9]
	ADD R9, R9, #1
	BAL _loop

	
@-display the decoded line

_display:

	LDR R0, =line
	BL printf

@-write decoded line to output.txt
 
	LDR R0, =line
	MOV R1, R4
	BL fputs
	CMP R0, #0
	BGT _read

	LDR R0, =error_on_put
	BL printf
	BAL _close
	
_close:

@ Close a file

	MOV R0, R4
	BL fclose
	CMP R0, #0
	BEQ _exit
	LDR R0, =error_on_close
	BL printf
	
_exit:
	
	MOV R7, #1
	SWI 0

.data
name:
.asciz "James Sutherlin\n"
path1:
.asciz "input.txt"
mode1:
.asciz "r"
path2:
.asciz "output.txt"
mode2:
.asciz "w"
error_on_open:
.asciz "Error found on open of file\n"
error_on_close:
.asciz "Error found on close of file\n"
error_on_get:
.asciz "Error found on gets\n"
error_on_put:
.asciz "Error found on puts\n"
line:
.ascii "                                                             "
end_of_line:
.byte 0
